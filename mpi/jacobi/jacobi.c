/*
 * Adapted from http://www.mcs.anl.gov/research/projects/mpi/tutorial/mpiexmpl/src/jacobicmpl/C/main.html
 */
#include <math.h>
#include <mpi.h>
#include <stdio.h>

/* This example handles a 12 x 12 mesh, on 4 processors only */
#define N 12
#define P 4
#define EPSILON 1.0e-2
#define MAXITER 100

int main(int argc, char* argv[]) {
    int i, j;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size != P) MPI_Abort(MPI_COMM_WORLD, 1);

    /* Read the data from the named file */
    double x[N][N];
    if (rank == 0) {
        FILE *fp;
        fp = fopen("in.dat", "r");
        if (!fp) MPI_Abort(MPI_COMM_WORLD, 1);
        /* This includes the top and bottom edge */
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++) {
                fscanf(fp, "%lf", &x[i][j]);
            }
            fscanf(fp, "\n");
        }
    }

    /* xlocal[][0] is lower ghost points, xlocal[][N+2] is upper */
    double xlocal[(N/size) + 2][N];
    double xnew[(N/size) + 2][N];

    MPI_Scatter(x[0], N * (N/size), MPI_DOUBLE,
                xlocal[1], N * (N/size), MPI_DOUBLE,
                0, MPI_COMM_WORLD);

    /* Top and bottom processes have one less row of interior points */
    int i_first = 1;
    int i_last  = N / size;
    if (rank == 0)        i_first++;
    if (rank == size - 1) i_last--;

    int iter = 0;
    double diffnorm, gdiffnorm;
    do {
        /* Send up unless I'm at the top, then receive from below */
        if (rank > 0)
            MPI_Send(xlocal[1], N, MPI_DOUBLE, rank - 1, 1, MPI_COMM_WORLD);
        if (rank < size - 1)
            MPI_Recv(xlocal[N/size + 1], N, MPI_DOUBLE, rank + 1, 1, MPI_COMM_WORLD, &status);
        /* Send down unless I'm at the bottom */
        if (rank < size - 1)
            MPI_Send(xlocal[N/size], N, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD);
        if (rank > 0)
            MPI_Recv(xlocal[0], N, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &status);

        /* Compute new values (but not on boundary) */
        iter++;
        diffnorm = 0.0;
        for (i = i_first; i <= i_last; i++) {
            for (j = 1; j < N - 1; j++) {
                xnew[i][j] = (xlocal[i][j+1] + xlocal[i][j-1] +
                              xlocal[i+1][j] + xlocal[i-1][j]) / 4.0;
                diffnorm += (xnew[i][j] - xlocal[i][j]) *
                            (xnew[i][j] - xlocal[i][j]);
            }
        }
        /* Only transfer the interior points */
        for (i = i_first; i <= i_last; i++)
            for (j = 1; j < N - 1; j++)
                xlocal[i][j] = xnew[i][j];

        MPI_Allreduce(&diffnorm, &gdiffnorm, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        gdiffnorm = sqrt(gdiffnorm);
        if (rank == 0) {
            printf("At iteration %d, diff is %e\n", iter, gdiffnorm);
        }
    } while (gdiffnorm > EPSILON && iter < MAXITER);

    /* Collect the data into x and print it */
    MPI_Gather(xlocal[1], N * (N/size), MPI_DOUBLE,
               x, N * (N/size), MPI_DOUBLE,
               0, MPI_COMM_WORLD);
    if (rank == 0) {
        printf("Final solution is\n");
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++) {
                printf("%f ", x[i][j]);
            }
            printf("\n");
        }
    }

    MPI_Finalize();
    return 0;
}