#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/string.hpp>
#include <iostream>
#include <sstream>
#include <string>
namespace mpi = boost::mpi;

int main() {
    mpi::environment env;
    mpi::communicator world;
    int tag = 1;

    if (world.rank() == 0) {
        std::cout << "Hello, world. I am " << world.rank() 
                  << " of " << world.size()
                  << " on " << env.processor_name() << std::endl;
        std::string msg;
        for (int i = 1; i < world.size(); i++) {
            mpi::status status = world.recv(mpi::any_source, tag, msg);
            std::cout << "Msg from " << status.source() << ": '" 
                      << msg << "'" << std::endl;
        }
    } else {
        std::stringstream buffer;
        buffer << "Hello, master. I am " << world.rank() 
               << " of " << world.size()
               << " on " << env.processor_name();
        world.send(0, tag, buffer.str());
    }

    return 0;
}