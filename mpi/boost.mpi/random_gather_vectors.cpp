// Copyright (C) 2006 Douglas Gregor <doug.gregor@gmail.com>

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

// An example using Boost.MPI's gather()

#include <boost/mpi.hpp>
#include <cstdlib>
#include <iostream>
#include <vector>
namespace mpi = boost::mpi;

int main() {
    mpi::environment env;
    mpi::communicator world;

    int N = 5;
    std::vector<int> my_numbers;
    std::srand(time(0) + world.rank());
    for (int i = 0; i < N; ++i)
        my_numbers.push_back(std::rand());

    if (world.rank() == 0) {
        std::vector<int> all_numbers;
        gather(world, &my_numbers[0], N, all_numbers, 0);
        for (auto number : all_numbers)
            std::cout << number << std::endl;
    } else {
        gather(world, &my_numbers[0], N, 0);
    }

    return 0;
}