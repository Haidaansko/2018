#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <iostream>
namespace mpi = boost::mpi;

int main() {
    mpi::environment env;
    mpi::communicator world;
    std::cout << "Hello, world. I am " << world.rank() << " of " << world.size()
              << " on " << env.processor_name() << std::endl;
    return 0;
}