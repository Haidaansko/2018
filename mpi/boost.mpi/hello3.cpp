#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/nonblocking.hpp>
#include <boost/serialization/string.hpp>
#include <iostream>
#include <string>
namespace mpi = boost::mpi;

int main() {
    mpi::environment env;
    mpi::communicator world;

    int rank = world.rank();
    int size = world.size();
    int prev = (rank + size - 1) % size;
    int next = (rank + 1) % size;
    mpi::request reqs[4];
    mpi::status stats[4];
    std::string in_msg[2];

    reqs[0] = world.irecv(prev, 1, in_msg[0]);
    reqs[1] = world.irecv(next, 2, in_msg[1]);

    std::string msg = "I am " + std::to_string(rank) + 
        " of " + std::to_string(size) + " on " + env.processor_name();
    std::string msg_prev = "Hello, prev. " + msg;
    std::string msg_next = "Hello, next. " + msg;

    reqs[2] = world.isend(next, 1, msg_next);
    reqs[3] = world.isend(prev, 2, msg_prev);

    mpi::wait_all(reqs, reqs + 4, stats);

    for (int i=0; i<2; i++) {
        std::cout << "[" << rank << "] Msg from " << stats[i].source() << ": '" 
            << in_msg[i] << "'" << std::endl;    
    }

    return 0;
}