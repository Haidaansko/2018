#include <mpi.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
    int rank, size, len, prev, next, tag1 = 1, tag2 = 2;
    char host[MPI_MAX_PROCESSOR_NAME];
    char in_buf1[50], in_buf2[50], out_buf1[50], out_buf2[50];
    MPI_Request reqs[4];
    MPI_Status stats[4];
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(host, &len);

    prev = (rank + size - 1) % size;
    next = (rank + 1) % size;
    
    MPI_Irecv(&in_buf1, 50, MPI_CHAR, prev, tag1, MPI_COMM_WORLD, &reqs[0]);
    MPI_Irecv(&in_buf2, 50, MPI_CHAR, next, tag2, MPI_COMM_WORLD, &reqs[1]);
    
    snprintf(out_buf1, 50, "Hello, next. I am %d of %d on %s", rank, size, host);
    snprintf(out_buf2, 50, "Hello, prev. I am %d of %d on %s", rank, size, host);
    MPI_Isend(&out_buf1, 50, MPI_CHAR, next, tag1, MPI_COMM_WORLD, &reqs[2]);
    MPI_Isend(&out_buf2, 50, MPI_CHAR, prev, tag2, MPI_COMM_WORLD, &reqs[3]);

    MPI_Waitall(4, reqs, stats);
        
    printf("[%d] Msg from %d: '%s'\n", rank, stats[0].MPI_SOURCE, in_buf1);
    printf("[%d] Msg from %d: '%s'\n", rank, stats[1].MPI_SOURCE, in_buf2);

    MPI_Finalize();
    return 0;
}