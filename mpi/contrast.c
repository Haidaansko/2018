#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    int width, height, rank, comm_size;
    int sum, my_sum, numpixels, my_count, i, val;
    unsigned char *pixels, *recvbuf;
    double rms, start_time;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

    /* Synchronize processes and mark the start time */
    MPI_Barrier(MPI_COMM_WORLD);
    start_time = MPI_Wtime();

    if (rank == 0) {
        width = 20480; height = 20480;
        numpixels = width * height;
        pixels = (unsigned char*) malloc(numpixels*sizeof(unsigned char));

        /* Load the image (replaced with random generation) */
        for (i = 0; i < numpixels; i++) pixels[i] = rand() % 255;
    }
    
    /* Broadcast numpixels to all the processes */
    MPI_Bcast(&numpixels, 1, MPI_INT, 0, MPI_COMM_WORLD);

    /* Calculate the number of pixels in each sub image */
    my_count = numpixels / comm_size;

    /* Scatter the image */
    recvbuf = (unsigned char*) malloc(my_count * sizeof(unsigned char));
    MPI_Scatter(pixels, my_count, MPI_UNSIGNED_CHAR, recvbuf, my_count, MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);

    /* Take the sum of the squares of the partial image */
    my_sum = 0;
    for (i = 0; i < my_count; i++) {
        my_sum += recvbuf[i] * recvbuf[i];
    }

    /* Find the global sum of the squares */
    MPI_Allreduce(&my_sum, &sum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    /* Calculate the root mean square */
    rms = sqrt ((double) sum / (double) numpixels);
    
    /* Do the contrast operation */
    for (i = 0; i < my_count; i++) {
        val = 2 * recvbuf[i] - rms;
        if (val < 0 )
            recvbuf[i] = 0;
        else if (val > 255)
            recvbuf[i] = 255;
        else
            recvbuf[i] = val;
    }

    /* Gather back to root */
    MPI_Gather(recvbuf, my_count, MPI_UNSIGNED_CHAR, pixels, my_count, MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);

    /* Dump the image in process 0 (skipped) */

    /* Calculate and print run time */
    if (rank == 0) {
        printf("TIME: %lf\n", MPI_Wtime() - start_time);
    }

    MPI_Finalize();
    return 0;
}
