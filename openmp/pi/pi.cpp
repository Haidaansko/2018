#include <chrono>
#include <cstdio>
#include <ctime>

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::duration<double> dsec;

int main() {
    auto start1 = Clock::now();
    std::clock_t start2 = std::clock();

    const int num_steps = 1E8;
    const double step = 1.0 / num_steps;
    double x, sum = 0.0;

    for (int i = 0; i < num_steps; i++) {
        x = (i + 0.5) * step;
        sum += 4.0 / (1.0 + x * x);
    }

    double pi = step * sum;

    dsec time1 = Clock::now() - start1;
    double time2 = (std::clock() - start2) / (double)CLOCKS_PER_SEC;

    printf("Pi = %.16f\n", pi);
    printf("Time (std::chrono): %.3f s\n", time1.count());
    printf("Time (std::clock): %.3f s\n", time2);

    return 0;
}