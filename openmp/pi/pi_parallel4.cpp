#include <cstdio>
#include <omp.h>

int main() {
    double start = omp_get_wtime();

    const int num_steps = 1E8;
    const double step = 1.0 / num_steps;
    double x, sum = 0.0;

    int num_threads = omp_get_max_threads();
    double sum_tmp[num_threads * 8];

    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        // padding array elements to avoid false sharing
        int pos = id * 8;
        sum_tmp[pos] = 0;
        #pragma omp for private(x) nowait
        for (int i = 0; i < num_steps; i++) {
            x = (i + 0.5) * step;
            sum_tmp[pos] += 4.0 / (1.0 + x * x);
        }
        #pragma omp atomic
        sum += sum_tmp[pos];
    }

    double pi = step * sum;

    printf("Pi = %.16f\n", pi);
    printf("Time: %.3f s\n", omp_get_wtime() - start);
    return 0;
}
