#include <cstdio>

int main() {
    #pragma omp parallel
    printf("Hello, Parallel World!\n");
    return 0;
}
