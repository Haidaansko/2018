#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE 2000000
#define THRESHOLD 200

void quicksort_tasks(int arr[], int low, int high) {
    int i = low;
    int j = high;
    int y = 0;
    int z = arr[(low + high) / 2];
    do {
        while (arr[i] < z) i++;
        while (arr[j] > z) j--;
        if (i <= j) {
            y = arr[i];
            arr[i] = arr[j];
            arr[j] = y;
            i++;
            j--;
        }
    } while (i <= j);

    if (high-low < THRESHOLD || (j-low < THRESHOLD || high-i < THRESHOLD)) {
        if (low < j)
            quicksort_tasks(arr, low, j);
        if (i < high)
            quicksort_tasks(arr, i, high);
    } else {

        #pragma omp task
        {
            quicksort_tasks(arr, low, j);
        }

        quicksort_tasks(arr, i, high);
    }
}

int main(void) {
    int array[SIZE] = {0};
    int i = 0;

    for (i = 0; i < SIZE; i++)
        array[i] = rand() % 10000000;

    double start = omp_get_wtime();
    #pragma omp parallel
    #pragma omp single
    {
        quicksort_tasks(array, 0, (SIZE - 1));
    }
    printf("Parallel Tasks: %f\n", omp_get_wtime() - start);


    return 0;
}
