#include <iostream>
#include <omp.h>
#include <random>
#include <thread>
#include <vector>

int main() {
    const unsigned N = 8000;
    const unsigned M = 8000;

    std::vector<std::vector<double>> A(N, std::vector<double>(M, 0));
    std::vector<double> x(M, 0), y(N, 0);

    #pragma omp parallel
    {
        int thread_id = omp_get_thread_num();
        if (thread_id == 0) {
            std::cout << "Number of threads = " << omp_get_num_threads() << "\n";
        }

        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_real_distribution<> dist(0, 1);
        #pragma omp for
        for (unsigned i = 0; i < N; i++) {
            double a = dist(e2);
            for (unsigned j = 0; j < M; j++) {
                A[i][j] = a * i + j;
            }
            y[i] = 0;
        }
        #pragma omp for
        for (unsigned j = 0; j < M; j++) {
            x[j] = dist(e2);
        }
    }

    double start, mult_time;

    // Serial version

    start = omp_get_wtime();

    for (unsigned i = 0; i < N; i++) {
        for (unsigned j = 0; j < M; j++) {
            y[i] += A[i][j] * x[j];
        }
    }

    mult_time = omp_get_wtime() - start;
    std::cout << "Serial Time: " << mult_time << "\n";

    std::fill(y.begin(), y.end(), 0);

    // Parallel version

    start = omp_get_wtime();

    #pragma omp parallel for
    for (unsigned i = 0; i < N; i++) {
        for (unsigned j = 0; j < M; j++) {
            y[i] += A[i][j] * x[j];
        }
    }

    mult_time = omp_get_wtime() - start;
    std::cout << "Parallel Time: " << mult_time << "\n";

    std::fill(y.begin(), y.end(), 0);

    // Parallel version 2

    start = omp_get_wtime();

    #pragma omp parallel for
    for (unsigned i = 0; i < N; i++) {
        double sum = 0.0;
        for (unsigned j = 0; j < M; j++) {
            sum += A[i][j] * x[j];
        }
        y[i] = sum;
    }

    mult_time = omp_get_wtime() - start;
    std::cout << "Parallel 2 Time: " << mult_time << "\n";
}
