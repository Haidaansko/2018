#include <algorithm>
#include <assert.h>
#include <iostream>
#include <string>
#include <thread>

void uppercase1(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(), ::toupper);
}

void uppercase2(std::string& s) {
    std::transform(s.begin(), s.end(), s.begin(), ::toupper);
}

void uppercase3(std::unique_ptr<std::string> s) {
    std::transform(s->begin(), s->end(), s->begin(), ::toupper);
    std::cout << *s << std::endl;
}

int main() {
    std::string s("Hello, Concurrent World!");

    std::thread my_thread1(uppercase1, s); // pass by value
    my_thread1.join();
    std::cout << s << std::endl;

    std::thread my_thread2(uppercase2, std::ref(s)); // pass by reference
    my_thread2.join();
    std::cout << s << std::endl;

    std::unique_ptr<std::string> s_ptr(new std::string("Hello, Concurrent World!"));
    std::thread my_thread3(uppercase3, std::move(s_ptr)); // move (transfer ownership)
    my_thread3.join();
    assert(!s_ptr);
}