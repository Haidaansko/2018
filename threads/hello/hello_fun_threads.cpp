#include <iostream>
#include <thread>
#include <vector>

void hello(unsigned id) {
    std::cout << id << ": Hello, Concurrent World!\n";
}

int main() {
    unsigned num = std::thread::hardware_concurrency();
    std::cout << "Concurrent threads: " << num << std::endl;
    std::vector<std::thread> threads;
    for (unsigned i = 0; i < num; ++i) {
        threads.push_back(std::thread(hello, i));
    }
    for (auto& thread : threads) {
        thread.join();
    }
}
