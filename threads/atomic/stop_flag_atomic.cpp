#include <atomic>
#include <iostream>
#include <thread>

std::atomic<bool> stop_flag;
int x, x1;

void run() {
    while (!stop_flag)
        ;
    x1 = x;
}

int main() {
    stop_flag = false;
    x = 0;
    x1 = 0;
    std::thread my_thread(run);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    x = 1;
    stop_flag = true;
    my_thread.join();
    std::cout << x1 << std::endl;
}