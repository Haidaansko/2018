#include <iostream>
#include <thread>

int x, y, zx, zy;

void threadA() {
    x = 1;
    zy = y;
}

void threadB() {
    y = 1;
    zx = x;
}

int main() {
    for (int i = 0; i < 1000000; ++i) {
        x = 0;
        y = 0;
        zx = 0;
        zy = 0;
        std::thread a(threadA);
        std::thread b(threadB);
        a.join();
        b.join();
        if (zx == 0 && zy == 0) {
            std::cout << "Impossible!" << std::endl;
        }
    }
}