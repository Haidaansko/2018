#include <chrono>
#include <condition_variable>
#include <mutex>
#include <thread>

class BoundedBuffer {
public:
    BoundedBuffer(int capacity)
        : capacity(capacity)
        , head(0)
        , tail(0)
        , count(0) {
        buffer = new int[capacity];
    }

    ~BoundedBuffer() {
        delete[] buffer;
    }

    void put(int item) {
        std::unique_lock<std::mutex> lock(mutex);

        not_full.wait(lock, [this]() { return count != capacity; });

        buffer[tail] = item;
        tail = (tail + 1) % capacity;
        ++count;

        not_empty.notify_one();
    }

    int take() {
        std::unique_lock<std::mutex> lock(mutex);

        not_empty.wait(lock, [this]() { return count != 0; });

        int item = buffer[head];
        head = (head + 1) % capacity;
        --count;

        not_full.notify_one();

        return item;
    }

private:
    int* buffer;
    int capacity;

    int head;
    int tail;
    int count;

    std::mutex mutex;
    std::condition_variable not_full;
    std::condition_variable not_empty;
};

void consumer(int id, int count, BoundedBuffer& buffer) {
    for (int i = 0; i < count; ++i) {
        int item = buffer.take();
        std::printf("Consumer %d took %d\n", id, item);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void producer(int id, int count, BoundedBuffer& buffer) {
    int start = (id - 1) * count + 1;
    int end = start + count;
    for (int i = start; i < end; ++i) {
        buffer.put(i);
        std::printf("Producer %d put %d\n", id, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

int main() {
    BoundedBuffer buffer(100);

    std::thread c1(consumer, 1, 50, std::ref(buffer));
    std::thread c2(consumer, 2, 50, std::ref(buffer));
    std::thread c3(consumer, 3, 50, std::ref(buffer));

    std::thread p1(producer, 1, 75, std::ref(buffer));
    std::thread p2(producer, 2, 75, std::ref(buffer));

    p1.join();
    p2.join();

    c1.join();
    c2.join();
    c3.join();

    return 0;
}