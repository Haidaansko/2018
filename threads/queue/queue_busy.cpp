#include <chrono>
#include <mutex>
#include <queue>
#include <thread>

#include <cstdio>

template <typename T>
class ThreadsafeQueue {
private:
    std::queue<T> queue;
    std::mutex mutex;

public:
    void put(const T& item) {
        std::lock_guard<std::mutex> lock(mutex);
        queue.push(item);
    }

    T take() {
        std::unique_lock<std::mutex> lock(mutex, std::defer_lock);
        while (true) {
            lock.lock();
            if (!queue.empty()) {
                T item = queue.front();
                queue.pop();
                return item;
            }
            lock.unlock();
            // add delay to decrease CPU usage (but problably increase run time)
            // std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }

    ThreadsafeQueue() = default;
    ThreadsafeQueue(const ThreadsafeQueue&) = delete;            // disable copying
    ThreadsafeQueue& operator=(const ThreadsafeQueue&) = delete; // disable assignment
};

void consumer(int id, ThreadsafeQueue<int>& queue) {
    for (int i = 0; i < 50; ++i) {
        int item = queue.take();
        std::printf("Consumer %d took %d\n", id, item);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void producer(int id, ThreadsafeQueue<int>& queue) {
    for (int i = 0; i < 75; ++i) {
        queue.put(i);
        std::printf("Producer %d put %d\n", id, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

int main() {
    ThreadsafeQueue<int> queue;

    std::thread c1(consumer, 0, std::ref(queue));
    std::thread c2(consumer, 1, std::ref(queue));
    std::thread c3(consumer, 2, std::ref(queue));
    std::thread p1(producer, 0, std::ref(queue));
    std::thread p2(producer, 1, std::ref(queue));

    c1.join();
    c2.join();
    c3.join();
    p1.join();
    p2.join();

    return 0;
}
