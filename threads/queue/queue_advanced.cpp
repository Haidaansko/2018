// An advanced thread-safe queue implementation from
// Anthony Williams. C++ Concurrency in Action: Practical Multithreading. Manning, 2012.

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

#include <cstdio>

template <typename T>
class ThreadsafeQueue {
private:
    mutable std::mutex mut;
    std::queue<T> data_queue;
    std::condition_variable data_cond;

public:
    ThreadsafeQueue() {
    }
    ThreadsafeQueue(ThreadsafeQueue const& other) {
        std::lock_guard<std::mutex> lk(other.mut);
        data_queue = other.data_queue;
    }

    void push(T new_value) {
        std::lock_guard<std::mutex> lk(mut);
        data_queue.push(new_value);
        data_cond.notify_one();
    }

    void wait_and_pop(T& value) {
        std::unique_lock<std::mutex> lk(mut);
        data_cond.wait(lk, [this] { return !data_queue.empty(); });
        value = data_queue.front();
        data_queue.pop();
    }

    std::shared_ptr<T> wait_and_pop() {
        std::unique_lock<std::mutex> lk(mut);
        data_cond.wait(lk, [this] { return !data_queue.empty(); });
        std::shared_ptr<T> res(std::make_shared<T>(data_queue.front()));
        data_queue.pop();
        return res;
    }

    bool try_pop(T& value) {
        std::lock_guard<std::mutex> lk(mut);
        if (data_queue.empty())
            return false;
        value = data_queue.front();
        data_queue.pop();
        return true;
    }

    std::shared_ptr<T> try_pop() {
        std::lock_guard<std::mutex> lk(mut);
        if (data_queue.empty())
            return std::shared_ptr<T>();
        std::shared_ptr<T> res(std::make_shared<T>(data_queue.front()));
        data_queue.pop();
        return res;
    }

    bool empty() const {
        std::lock_guard<std::mutex> lk(mut);
        return data_queue.empty();
    }
};

void consumer(int id, ThreadsafeQueue<int>& queue) {
    for (int i = 0; i < 50; ++i) {
        int value = *queue.wait_and_pop();
        std::printf("Consumer %d took %d\n", id, value);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void producer(int id, ThreadsafeQueue<int>& queue) {
    for (int i = 0; i < 75; ++i) {
        queue.push(i);
        std::printf("Producer %d put %d\n", id, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

int main() {
    ThreadsafeQueue<int> queue;

    std::thread c1(consumer, 0, std::ref(queue));
    std::thread c2(consumer, 1, std::ref(queue));
    std::thread c3(consumer, 2, std::ref(queue));
    std::thread p1(producer, 0, std::ref(queue));
    std::thread p2(producer, 1, std::ref(queue));

    c1.join();
    c2.join();
    c3.join();
    p1.join();
    p2.join();

    return 0;
}
