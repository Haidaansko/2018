#include <chrono>
#include <future>
#include <iostream>
#include <math.h>

double stepA(double x) {
    std::cout << "Running step A..." << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Completed step A" << std::endl;
    return x * 2;
}

double stepB(double x) {
    std::cout << "Running step B...\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Completed step B\n";
    return x + 2;
}

double stepC(double x) {
    std::cout << "Running step C...\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Completed step C\n";
    return x - 10;
}

double stepD(double x, double y) {
    std::cout << "Running step D..." << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Completed step D" << std::endl;
    return x + y;
}

int main() {
    auto start = std::chrono::high_resolution_clock::now();

    std::shared_future<double> resA = std::async(stepA, 10).share();

    std::future<double> resB =
        std::async(std::launch::async,
                   [](std::shared_future<double> resA) { return stepB(resA.get()); }, resA);

    std::future<double> resC =
        std::async(std::launch::async,
                   [](std::shared_future<double> resA) { return stepC(resA.get()); }, resA);

    // doesn't compile in MS VS 2015 =(
    std::future<double> resD =
        std::async([](std::future<double> resB,
                      std::future<double> resC) { return stepD(resB.get(), resC.get()); },
                   std::move(resB), std::move(resC));

    std::cout << "Result: " << resD.get() << std::endl;

    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "Program took " << std::chrono::duration<double>(stop - start).count()
              << " seconds" << std::endl;
}