#include <chrono>
#include <future>
#include <iostream>
#include <math.h>

double calculate(double x) {
    std::cout << "calculating" << std::endl;
    if (x < 0) {
        throw std::out_of_range("x<0");
    }
    std::this_thread::sleep_for(std::chrono::seconds(1));
    return sqrt(x);
}

void do_other_stuff() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "done stuff" << std::endl;
}

void async_test() {
    auto start = std::chrono::high_resolution_clock::now();

    std::future<double> res = std::async(calculate, 10);
    std::future<double> res2 = std::async(calculate, -10);
    do_other_stuff();
    std::cout << "result is " << res.get() << std::endl;

    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "test took " << std::chrono::duration<double>(stop - start).count() << " seconds"
              << std::endl;
}

void async_policy_test(std::launch policy) {
    auto start = std::chrono::high_resolution_clock::now();

    std::future<double> res = std::async(policy, calculate, 10);
    std::future<double> res2 = std::async(policy, calculate, -10);
    do_other_stuff();
    std::cout << "result is " << res.get() << std::endl;

    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "test took " << std::chrono::duration<double>(stop - start).count() << " seconds"
              << std::endl;
}

int main() {
    std::cout << "DEFAULT POLICY" << std::endl;
    async_test();
    std::cout << std::endl;
    std::cout << "ASYNC POLICY" << std::endl;
    async_policy_test(std::launch::async);
    std::cout << std::endl;
    std::cout << "DEFERRED POLICY" << std::endl;
    async_policy_test(std::launch::deferred);
}