#include <future>
#include <iostream>
#include <math.h>

double calculate(double x) {
    if (x < 0) {
        throw std::out_of_range("x<0");
    }
    return sqrt(x);
}

void background_thread(std::promise<double> promise, double x) {
    try {
        promise.set_value(calculate(x)); // comment to get broken promise error
    } catch (const std::exception&) {
        promise.set_exception(std::current_exception());
    }
    // do some other work...
}

int main() {
    std::promise<double> promise;
    std::future<double> res = promise.get_future();
    std::thread t(background_thread, std::move(promise), 10);
    try {
        std::cout << res.get() << std::endl;
    } catch (const std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
    }
    t.join();
}