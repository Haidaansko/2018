#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#define TIMING

#ifdef TIMING
#define INIT_TIMER auto start = std::chrono::high_resolution_clock::now();
#define RESET_TIMER start = std::chrono::high_resolution_clock::now();
#define STOP_TIMER(name)                                                                           \
    std::cout << name << " RUNTIME: "                                                              \
              << std::chrono::duration_cast<std::chrono::milliseconds>(                            \
                     std::chrono::high_resolution_clock::now() - start)                            \
                     .count()                                                                      \
              << " ms " << std::endl;
#else
#define INIT_TIMER
#define RESET_TIMER
#define STOP_TIMER(name)
#endif

// TEST INPUT FILES (uncompress after download):
// * data-part.xml (45 MB): https://yadi.sk/d/X98gF1Zh3EGRzQ
// * data.xml (490 MB): https://yadi.sk/d/CQA44OB93EGTqK

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::fprintf(stderr, "Usage: %s file needle\n", argv[0]);
        return 1;
    }
    std::ifstream fs(argv[1]);
    std::string needle = argv[2];

    INIT_TIMER

    std::vector<std::string> lines;
    for (std::string line; std::getline(fs, line);) {
        lines.push_back(line);
    }

    // STOP_TIMER("READ")
    // RESET_TIMER

    int count = 0;
    for (std::string str : lines) {
        std::transform(str.begin(), str.end(), str.begin(), ::tolower);
        if (str.find(needle) != std::string::npos) {
            // std::cout << str << std::endl;
            count++;
        }
    }
    std::cout << "Found " << count << " matches" << std::endl;

    STOP_TIMER("TOTAL")
    // STOP_TIMER("SEARCH")
}