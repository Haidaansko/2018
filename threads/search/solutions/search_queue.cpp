#include <algorithm>
#include <chrono>
#include <fstream>
#include <future>
#include <iostream>
#include <queue>
#include <string>
#include <thread>
#include <vector>

#define TIMING

#ifdef TIMING
#define INIT_TIMER auto start = std::chrono::high_resolution_clock::now();
#define RESET_TIMER start = std::chrono::high_resolution_clock::now();
#define STOP_TIMER(name)                                                                           \
    std::cout << name << " RUNTIME: "                                                              \
              << std::chrono::duration_cast<std::chrono::milliseconds>(                            \
                     std::chrono::high_resolution_clock::now() - start)                            \
                     .count()                                                                      \
              << " ms " << std::endl;
#else
#define INIT_TIMER
#define RESET_TIMER
#define STOP_TIMER(name)
#endif

template <typename T>
class ThreadsafeQueue {
private:
    std::queue<T> queue;
    std::mutex mutex;
    std::condition_variable cond;

public:
    void put(const T& item) {
        std::unique_lock<std::mutex> ulock(mutex);
        queue.push(item);
        ulock.unlock();
        cond.notify_one();
    }

    T take() {
        std::unique_lock<std::mutex> ulock(mutex);
        while (queue.empty())
            cond.wait(ulock);
        T val = queue.front();
        queue.pop();
        return val;
    }

    void take(T& item) {
        std::unique_lock<std::mutex> ulock(mutex);
        while (queue.empty())
            cond.wait(ulock);
        item = queue.front();
        queue.pop();
    }

    ThreadsafeQueue() = default;
    ThreadsafeQueue(const ThreadsafeQueue&) = delete;            // disable copying
    ThreadsafeQueue& operator=(const ThreadsafeQueue&) = delete; // disable assignment
};

std::vector<std::string> search(std::string needle, ThreadsafeQueue<std::string>& queue) {
    std::vector<std::string> results;
    std::string str;
    while ((str = queue.take()) != "POISON") {
        std::transform(str.begin(), str.end(), str.begin(), ::tolower);
        if (str.find(needle) != std::string::npos) {
            results.push_back(str);
        }
    }
    return results;
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::fprintf(stderr, "Usage: %s file needle\n", argv[0]);
        return 1;
    }
    std::ifstream fs(argv[1]);
    std::string needle = argv[2];

    INIT_TIMER

    ThreadsafeQueue<std::string> queue;
    int num_proc = std::thread::hardware_concurrency();
    std::cout << "Using " << num_proc << " threads" << std::endl;
    std::vector<std::future<std::vector<std::string>>> futures;
    for (int i = 0; i < num_proc; i++) {
        futures.push_back(std::async(std::launch::async, search, needle, std::ref(queue)));
    }

    for (std::string line; std::getline(fs, line);) {
        queue.put(line);
    }

    for (int i = 0; i < num_proc; i++) {
        queue.put("POISON");
    }

    int count = 0;
    for (int i = 0; i < num_proc; i++) {
        std::vector<std::string> res = futures.at(i).get();
        // for (std::string str : res) {
        //   std::cout << str << std::endl;
        //}
        count += res.size();
    }
    std::cout << "Found " << count << " matches" << std::endl;

    STOP_TIMER("READ/SEARCH")
}