#include <algorithm>
#include <chrono>
#include <fstream>
#include <future>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#define TIMING

#ifdef TIMING
#define INIT_TIMER auto start = std::chrono::high_resolution_clock::now();
#define RESET_TIMER start = std::chrono::high_resolution_clock::now();
#define STOP_TIMER(name)                                                                           \
    std::cout << name << " RUNTIME: "                                                              \
              << std::chrono::duration_cast<std::chrono::milliseconds>(                            \
                     std::chrono::high_resolution_clock::now() - start)                            \
                     .count()                                                                      \
              << " ms " << std::endl;
#else
#define INIT_TIMER
#define RESET_TIMER
#define STOP_TIMER(name)
#endif

std::vector<int> search(std::string needle, const std::vector<std::string>& lines, int from,
                        int to) {
    std::vector<int> results;
    for (int i = from; i < to; i++) {
        std::string str = lines.at(i);
        std::transform(str.begin(), str.end(), str.begin(), ::tolower);
        if (str.find(needle) != std::string::npos) {
            results.push_back(i);
        }
    }
    return results;
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::fprintf(stderr, "Usage: %s file needle\n", argv[0]);
        return 1;
    }
    std::ifstream fs(argv[1]);
    std::string needle = argv[2];

    INIT_TIMER

    std::vector<std::string> lines;
    for (std::string line; std::getline(fs, line);) {
        lines.push_back(line);
    }

    STOP_TIMER("READ")
    RESET_TIMER

    int num_proc = std::thread::hardware_concurrency();
    std::cout << "Using " << num_proc << " threads" << std::endl;
    int part_size = lines.size() / num_proc;

    std::vector<std::future<std::vector<int>>> futures;
    for (int i = 0; i < num_proc; i++) {
        futures.push_back(std::async(std::launch::async, search, needle, std::ref(lines),
                                     part_size * i,
                                     (i != num_proc - 1) ? part_size * (i + 1) : lines.size()));
    }

    int count = 0;
    for (int i = 0; i < num_proc; i++) {
        std::vector<int> res = futures.at(i).get();
        // for (int pos : res) {
        //    std::cout << lines.at(pos) << std::endl;
        //}
        count += res.size();
    }
    std::cout << "Found " << count << " matches" << std::endl;

    STOP_TIMER("SEARCH")
}