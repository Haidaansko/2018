#include <chrono>
#include <cstdio>
#include <iostream>
#include <mutex>
#include <thread>

class Account {
public:
    Account(int id, int balance)
        : id(id)
        , balance(balance)
        , m() {
    }

    int getId() const {
        return id;
    }

    int getBalance() const {
        return balance;
    }

    void deposit(int amount) {
        balance += amount;
    }

    bool withdraw(int amount) {
        if (balance >= amount) {
            balance -= amount;
            return true;
        } else {
            return false;
        }
    }

    std::mutex& getMutex() {
        return m;
    }

private:
    int id;
    int balance;
    std::mutex m;
};

void transfer(int client_id, Account& from, Account& to, int amount) {
    if (from.withdraw(amount)) {
        std::printf("%d: withdraw %d OK\n", client_id, amount);
        to.deposit(amount);
        std::printf("%d: deposit %d OK\n", client_id, amount);
    } else {
        std::printf("%d: withdraw %d ERROR\n", client_id, amount);
    }
}

void client(int client_id, Account& from, Account& to, int amount) {
    if (from.getId() > to.getId()) {
        std::unique_lock<std::mutex> lock_from(from.getMutex());
        std::unique_lock<std::mutex> lock_to(to.getMutex());
        transfer(client_id, from, to, amount);
    } else {
        std::unique_lock<std::mutex> lock_to(to.getMutex());
        std::unique_lock<std::mutex> lock_from(from.getMutex());
        transfer(client_id, from, to, amount);
    }
}

int main(int argc, char* argv[]) {
    Account a(1, 100);
    Account b(2, 100);

    std::thread t1(client, 1, std::ref(a), std::ref(b), 10);
    std::thread t2(client, 2, std::ref(b), std::ref(a), 20);

    t1.join();
    t2.join();

    std::cout << "balance a = " << a.getBalance() << "\n";
    std::cout << "balance b = " << b.getBalance() << "\n";
}