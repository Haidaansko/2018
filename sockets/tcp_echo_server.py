#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('', 9876))
sock.listen(1)
conn, addr = sock.accept()

print 'Connected:', addr

while True:
    data = conn.recv(1024)
    if not data:
        break
    print 'Message:', data.strip()
    conn.send(data.upper())

conn.close()