import java.net.*;
import java.io.*;

public class TCPEchoServer {
    public static void main (String args[]) throws IOException {
        int serverPort = 9876;
        ServerSocket listenSocket = null;
        try {
            listenSocket = new ServerSocket(serverPort);
            while (true) {
                Socket clientSocket = listenSocket.accept();
                serveClient(clientSocket);
            }
        } catch (IOException e) { e.printStackTrace();
        } finally { if (listenSocket != null) listenSocket.close(); }
    }

    private static void serveClient(Socket clientSocket) {
        System.out.println("Start serving client");
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

            String data;
            while ((data = in.readLine()) != null) {
                System.out.println("Message: " + data);
                out.println(data.toUpperCase());
            }

            in.close();
            out.close();
        } catch (IOException e) { e.printStackTrace();
        } finally { try { clientSocket.close(); } catch (IOException e) {}}
        System.out.println("End serving client");
    }
}
