import java.net.*;
import java.io.*;

public class UDPEchoServer {

    public static void main(String args[]) {
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket(6789);
            DatagramPacket request = new DatagramPacket(new byte[1024], 1024);
            while (true) {
                socket.receive(request);
                String message = new String(request.getData(), 0, request.getLength());
                System.out.println(request.getAddress() + ":" + request.getPort() + "\t" + message);
                DatagramPacket reply = new DatagramPacket(message.toUpperCase().getBytes(),
                        request.getLength(), request.getAddress(), request.getPort());
                socket.send(reply);
            }
        } catch (IOException e) { e.printStackTrace();
        } finally { if (socket != null) socket.close(); }
    }
}
