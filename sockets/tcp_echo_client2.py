#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import sys
import time

host = sys.argv[1]
port = int(sys.argv[2])
msg = sys.argv[3]

s = None
success = False
while not success:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((host, port))
        print 'Connected'
    except socket.error as err:
        print 'Error1:', err
        time.sleep(3)
        continue

    try:
        s.sendall(msg + '\n')
        reply = ''
        while True:
            data = s.recv(1024)
            if not data:
                raise RuntimeError("Disconnected")
            reply += data
            if data.endswith('\n'):
                print 'Received', reply.strip()
                success = True
                break
    except (RuntimeError, socket.error) as err:
        print 'Error2:', err
        s.close()
s.close()