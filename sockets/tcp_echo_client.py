#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import sys

host = sys.argv[1]
port = int(sys.argv[2])
msg = sys.argv[3]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))
s.sendall(msg + '\n')
reply = ''
while True:
    data = s.recv(1024)
    reply += data
    if data.endswith('\n'):
        break
print 'Received', reply.strip()
s.close()