#include <stdio.h>
#include <cuda.h>

__global__ void hello () {
    int myID = ( blockIdx.y * gridDim.x + blockIdx.x ) * blockDim.x * blockDim.y 
               + threadIdx.y * blockDim.x + threadIdx.x; 
    printf ("Hello world from %i\n", myID);
}

int main () {
    dim3 grid(8, 4);
    dim3 block(4, 2);
    hello <<< grid, block >>> ();
    cudaDeviceSynchronize();
    return 0;
}