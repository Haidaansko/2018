#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

static const int BLOCK_SIZE = 256;
static const int N = 2000;

__global__ void vadd (int *a, int *b, int *c, int N) {
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if (id < N) {
        c[id] = a[id] + b[id];
    }
}

int main () {
    int *a, *b, *c;
    int i;

    // allocate unified memory
    cudaMallocManaged((void **) &a, N * sizeof(int));
    cudaMallocManaged((void **) &b, N * sizeof(int));
    cudaMallocManaged((void **) &c, N * sizeof(int));

    // initialize input vectors
    for (i = 0; i < N; i++) {
        a[i] = rand () % 10000;
        b[i] = rand () % 10000;
    }

    // run kernel
    int grid = ceil(N * 1.0 / BLOCK_SIZE);
    vadd <<< grid, BLOCK_SIZE >>> (a, b, c, N);
    cudaDeviceSynchronize();

    // check results
    for (i = 0; i < N; i++) {
        if (c[i] != a[i] + b[i]) {
            printf("Error at index %i : %i != %i\n", i, c[i], a[i] + b[i]);
        }
    }

    // free unified memory
    cudaFree(a);
    cudaFree(b);
    cudaFree(c);
    
    // clean up device resources
    cudaDeviceReset();

    return 0;
}
